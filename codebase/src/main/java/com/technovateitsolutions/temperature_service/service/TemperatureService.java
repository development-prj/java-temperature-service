package com.technovateitsolutions.temperature_service.service;

import com.technovateitsolutions.temperature_service.dto.SunriseSunsetDTO;
import com.technovateitsolutions.temperature_service.dto.TwilightType;
import com.technovateitsolutions.temperature_service.exception.InvalidLocationException;
import com.technovateitsolutions.temperature_service.utils.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Service
public class TemperatureService implements ITemperatureService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TemperatureService.class);

    @Autowired
    private ISunriseSunsetService sunriseSunsetService;

    @Autowired
    private IPredictionService<Double> predictionService;

    private final static double MIN_COLOR_SCREEN_VALUE = 2700d;
    private final static double MAX_COLOR_SCREEN_VALUE = 6000d;
    /**
     * Latitudes are supposed to be from -90 degrees to 90 degrees, but areas very near to the poles are not indexable.
     * So exact limits, as specified by EPSG:900913 / EPSG:3785 / OSGEO:41001 are the following:
     *     Valid latitudes are from -85.05112878 to 85.05112878 degrees.
     *     Valid longitudes are from -180 to 180 degrees.
     */
    private final static float MIN_LATITUDE_VALUE = -85.05112878f;
    private final static float MAX_LATITUDE_VALUE = 85.05112878f;
    private final static float MIN_LONGITUDE_VALUE = -180;
    private final static float MAX_LONGITUDE_VALUE = 180;

    private final static double SECONDS_OF_A_DAY = 24 * 3600;

    @Value("${useRequestTime}")
    private boolean USE_REQUEST_TIME;
    @Value("${mockedRequestTimePattern}")
    private String MOCKED_REQUEST_TIME_PATTERN;
    @Value("${mockedRequestTime}")
    private String MOCKED_REQUEST_TIME;

    public Optional<Double> getScreenColorByLocation(float latitude, float longitude) {
        return getScreenColorByLocation(latitude, longitude, TwilightType.CIVIL);
    }

    public Optional<Double> getScreenColorByLocation(float latitude, float longitude, TwilightType twilightType) {
        Supplier<LocalDateTime> requestTimeSupplier = () -> {
            if (USE_REQUEST_TIME) {
                return LocalDateTime.now(ZoneOffset.UTC);
            }
            return LocalDateTime.parse(MOCKED_REQUEST_TIME, DateTimeFormatter.ofPattern(MOCKED_REQUEST_TIME_PATTERN));
        };

        LocalDateTime requestTime = requestTimeSupplier.get();
        validateLocation(latitude, longitude);

        SunriseSunsetDTO sunriseSunsetDTO = sunriseSunsetService.getTodaySunriseSunsetBasedOnLocation(latitude, longitude);
        sunriseSunsetDTO.setLatitude(latitude);
        sunriseSunsetDTO.setLongitude(longitude);
        sunriseSunsetDTO.setCurrentTime(requestTime);
        if (twilightType == null) {
            twilightType = TwilightType.CIVIL;
        }
        sunriseSunsetDTO.setTwilightPreference(twilightType);

        return getScreenColor(sunriseSunsetDTO);
    }

    /**
     * Validates lat and longitude params.
     *
     * @param latitude latitude value
     * @param longitude longitude value
     * @throws InvalidLocationException in case input is invalid based on previous explained business rules
     */
    private void validateLocation(float latitude, float longitude) {
        if (MIN_LATITUDE_VALUE <= latitude && latitude <= MAX_LATITUDE_VALUE &&
            MIN_LONGITUDE_VALUE <= longitude && longitude <= MAX_LONGITUDE_VALUE
        ) {
            return;
        }
        throw new InvalidLocationException(String.format("Invalid location parameters: %f %f", latitude, longitude));
    }

    private Optional<Double> getScreenColor(SunriseSunsetDTO sunriseSunsetDTO) {
        if (sunriseSunsetDTO == null || sunriseSunsetDTO.getCurrentTime() == null) {
            return Optional.empty();
        }

        // in this case we know for sure we have the request time, so we don't check for isPresent of Optional
        double currentTimeOfDayInSeconds = DateTimeUtils.datetimeToElapsedSecondsOfDay(sunriseSunsetDTO.getCurrentTime()).get();

        LocalDateTime twilightBeginTime;
        LocalDateTime twilightEndTime;

        // detect which type of twilight should be used, with default CIVIL
        switch (sunriseSunsetDTO.getTwilightPreference()) {
            case NAUTICAL:
                twilightBeginTime = sunriseSunsetDTO.getNauticalTwilightBeginTime();
                twilightEndTime = sunriseSunsetDTO.getNauticalTwilightEndTime();
                break;
            case ASTRONOMICAL:
                twilightBeginTime = sunriseSunsetDTO.getAstronomicalTwilightBeginTime();
                twilightEndTime = sunriseSunsetDTO.getAstronomicalTwilightEndTime();
                break;
            default:
                twilightBeginTime = sunriseSunsetDTO.getCivilTwilightBeginTime();
                twilightEndTime = sunriseSunsetDTO.getCivilTwilightEndTime();
        }

        // applies a function on the last two elements of a list and replaces them in the list
        Consumer<List<Double>> recomputeList = (list -> {
           if (list == null || list.size() < 2) {
               return;
           }
           double lastButOne = list.get(list.size()-2);
           double last = list.get(list.size()-1);

           // if second elem is less than first, offset it's value by SECONDS_OF_A_DAY
           Double[] results = offsetSecondsByADay(lastButOne, last);
           list.set(list.size()-2, results[0]);
           list.set(list.size()-1, results[1]);
        });

        // this will hold as follows [twilightBegin, sunrise, sunrise + diff(sunrise, twilightBegin), sunset - diff(twilightEnd, sunset),  sunset, twilightEnd]
        // will keep nr of seconds of max values [0, SECONDS_OF_A_DAY * 2], meaning the seconds elapsed from begging of first day
        List<Double> pointsOfInterestInSeconds = new ArrayList<>(6);

        // add first two values to the list: [twilightBegin, sunrise]
        pointsOfInterestInSeconds.add(DateTimeUtils.datetimeToElapsedSecondsOfDay(twilightBeginTime).get());
        pointsOfInterestInSeconds.add(DateTimeUtils.datetimeToElapsedSecondsOfDay(sunriseSunsetDTO.getSunriseTime()).get());
        // makes sure to offset last value by SECONDS_OF_A_DAY, if UTC date changed
        recomputeList.accept(pointsOfInterestInSeconds);

        // add to the list: [sunrise + diff(sunrise, twilightBegin)]
        pointsOfInterestInSeconds.add(pointsOfInterestInSeconds.get(1) + (pointsOfInterestInSeconds.get(1) - pointsOfInterestInSeconds.get(0)));
        // makes sure to offset last value by SECONDS_OF_A_DAY, if UTC date changed
        recomputeList.accept(pointsOfInterestInSeconds);

        // sunset time in seconds
        double fifthPointOfInterestInSeconds = DateTimeUtils.datetimeToElapsedSecondsOfDay(sunriseSunsetDTO.getSunsetTime()).get();
        // twilight end time in seconds
        double sixthPointOfInterestInSeconds = DateTimeUtils.datetimeToElapsedSecondsOfDay(twilightEndTime).get();

        // makes sure to offset sunset time in seconds by SECONDS_OF_A_DAY, if UTC date changed between [twilight begin time, sunset]
        if (sunriseSunsetDTO.getSunsetTime().getDayOfMonth() != twilightBeginTime.getDayOfMonth()) {
            fifthPointOfInterestInSeconds = SECONDS_OF_A_DAY + fifthPointOfInterestInSeconds;
        }
        // makes sure to offset twilight end time in seconds in seconds by SECONDS_OF_A_DAY, if UTC date changed between [twilight begin time, twilight end time]
        if (twilightEndTime.getDayOfMonth() != twilightBeginTime.getDayOfMonth()) {
            sixthPointOfInterestInSeconds = SECONDS_OF_A_DAY + sixthPointOfInterestInSeconds;
        }
        // computes sunset - diff(twilightEnd, sunset)
        var forthPointOfInterestInSeconds = fifthPointOfInterestInSeconds - (sixthPointOfInterestInSeconds - fifthPointOfInterestInSeconds);
        // add last three values to the list: [sunset - diff(twilightEnd, sunset),  sunset, twilightEnd]
        pointsOfInterestInSeconds.add(forthPointOfInterestInSeconds);
        pointsOfInterestInSeconds.add(fifthPointOfInterestInSeconds);
        pointsOfInterestInSeconds.add(sixthPointOfInterestInSeconds);

        // if the UTC date changed between twilight begin and twilight end and request time is on the second day, increment it by SECONDS_OF_A_DAY
        if (twilightBeginTime.getDayOfMonth() != twilightEndTime.getDayOfMonth() && sunriseSunsetDTO.getCurrentTime().getDayOfMonth() == twilightEndTime.getDayOfMonth()) {
            currentTimeOfDayInSeconds = SECONDS_OF_A_DAY + currentTimeOfDayInSeconds;
        }

        // requested time is on daytime, so we return max value
        if (pointsOfInterestInSeconds.get(2) <= currentTimeOfDayInSeconds && currentTimeOfDayInSeconds <= pointsOfInterestInSeconds.get(3)) {
            return Optional.of(MAX_COLOR_SCREEN_VALUE);
        // requested time is on night time, so we return min value
        } else if (currentTimeOfDayInSeconds <= pointsOfInterestInSeconds.get(0) || currentTimeOfDayInSeconds >= pointsOfInterestInSeconds.get(5)) {
            return Optional.of(MIN_COLOR_SCREEN_VALUE);
        }

        // requested time is during sunrise, so we predict a value in interval [MIN, MAX]
        if (currentTimeOfDayInSeconds > pointsOfInterestInSeconds.get(0) && currentTimeOfDayInSeconds < pointsOfInterestInSeconds.get(2)) {
            return predictionService.predict(
                    new Double[] {pointsOfInterestInSeconds.get(0), pointsOfInterestInSeconds.get(2)},
                    new Double[] {MIN_COLOR_SCREEN_VALUE, MAX_COLOR_SCREEN_VALUE},
                    currentTimeOfDayInSeconds);
        // requested time is during sunshine, so we predict a value in interval [MAX, MIN]
        } else {
            return predictionService.predict(
                    new Double[] {pointsOfInterestInSeconds.get(3), pointsOfInterestInSeconds.get(5)},
                    new Double[] {MAX_COLOR_SCREEN_VALUE, MIN_COLOR_SCREEN_VALUE},
                    currentTimeOfDayInSeconds);
        }
    }

    /**
     * if second number is less than first, increment it by SECONDS_OF_A_DAY constant value
     * @param first first number
     * @param second second number
     * @return array with both number, first unmodified and second possibly modified
     */
    private Double[] offsetSecondsByADay(double first, double second) {
        if (second < first) {
            return new Double[] {first, SECONDS_OF_A_DAY + second};
        }
        return new Double[] {first, second};
    }
}
