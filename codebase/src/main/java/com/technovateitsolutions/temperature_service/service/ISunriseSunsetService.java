package com.technovateitsolutions.temperature_service.service;

import com.technovateitsolutions.temperature_service.dto.SunriseSunsetDTO;

public interface ISunriseSunsetService {
    SunriseSunsetDTO getTodaySunriseSunsetBasedOnLocation(float latitude, float longitude);
}
