package com.technovateitsolutions.temperature_service.service;

import java.util.Optional;

public interface IPredictionService<T> {
    /**
     * Requires N points definded by their (X,Y) values, and an X value for which we need to compute Y
     * @param xValues known X values
     * @param yValues known Y values for previous X values
     * @param inputXValue X value for which to compute Y value
     * @return computed Y value
     */
    Optional<T> predict(T[] xValues, T[] yValues, T inputXValue);
}
