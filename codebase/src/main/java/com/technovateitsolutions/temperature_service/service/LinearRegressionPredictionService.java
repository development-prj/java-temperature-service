package com.technovateitsolutions.temperature_service.service;

import org.apache.commons.math.stat.regression.SimpleRegression;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.Optional;
import java.util.stream.IntStream;

@Service
public class LinearRegressionPredictionService implements IPredictionService<Double> {
    private static final String DECIMAL_FORMAT_PATTERN = "#.##";

    @Override
    public Optional<Double> predict(Double[] xValues, Double[] yValues, Double inputXValue) {
        var isValidInput = isValidInput(xValues, yValues);
        if (!isValidInput) {
            return Optional.empty();
        }

        var regression = new SimpleRegression();
        IntStream.range(0, xValues.length).forEach(i -> {
            regression.addData(xValues[i], yValues[i]);
        });
        return Optional.of(Double.parseDouble(new DecimalFormat(DECIMAL_FORMAT_PATTERN).format(regression.predict(inputXValue))));
    }

    private boolean isValidInput(Double[] xValues, Double[] yValues) {
        return xValues != null && yValues != null && xValues.length == yValues.length && xValues.length == 2;
    }
}
