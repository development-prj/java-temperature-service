package com.technovateitsolutions.temperature_service.service;

import com.technovateitsolutions.temperature_service.dto.TwilightType;

import java.util.Optional;

public interface ITemperatureService {
    Optional<Double> getScreenColorByLocation(float latitude, float longitude);

    Optional<Double> getScreenColorByLocation(float latitude, float longitude, TwilightType twilightType);
}