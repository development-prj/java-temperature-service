package com.technovateitsolutions.temperature_service.service;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.technovateitsolutions.temperature_service.dto.SunriseSunsetDTO;
import com.technovateitsolutions.temperature_service.exception.InternalServerError;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

@Service
public class SunriseSunsetService implements ISunriseSunsetService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SunriseSunsetService.class);

    @Value("${sunrise-service.base-url}")
    private String SERVICE_BASEURL;
    @Value("${sunrise-service.formatted-response}")
    private int SERVICE_FORMATTED_RESPONSE;

    // The following two needed to be constant due to being used in annotations, compered to previous two properties
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'+00:00'";
    private static final String TIMEZONE = "UTC";

    @Data
    static class APIResponsePOJO implements Serializable {
        @JsonProperty("results")
        private APIResultPOJO result;
        private String status;
    }

    @Data
    static class APIResultPOJO implements Serializable {
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN, timezone = TIMEZONE)
        private LocalDateTime sunrise;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN, timezone = TIMEZONE)
        private LocalDateTime sunset;
        @JsonProperty("civil_twilight_begin")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN, timezone = TIMEZONE)
        private LocalDateTime civilTwilightBeginTime;
        @JsonProperty("civil_twilight_end")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN, timezone = TIMEZONE)
        private LocalDateTime civilTwilightEndTime;
        @JsonProperty("nautical_twilight_begin")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN, timezone = TIMEZONE)
        private LocalDateTime nauticalTwilightBeginTime;
        @JsonProperty("nautical_twilight_end")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN, timezone = TIMEZONE)
        private LocalDateTime nauticalTwilightEndTime;
        @JsonProperty("astronomical_twilight_begin")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN, timezone = TIMEZONE)
        private LocalDateTime astronomicalTwilightBeginTime;
        @JsonProperty("astronomical_twilight_end")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_TIME_PATTERN, timezone = TIMEZONE)
        private LocalDateTime astronomicalTwilightEndTime;
    }

    public SunriseSunsetDTO getTodaySunriseSunsetBasedOnLocation(float latitude, float longitude) {
        RestTemplate client = new RestTemplate();
        try {
            ResponseEntity<APIResponsePOJO> responsePOJOEntity = client.getForEntity(
                    String.format("%sjson?lat=%f&lng=%f&date=today&formatted=%d", SERVICE_BASEURL, latitude, longitude, SERVICE_FORMATTED_RESPONSE),
                    APIResponsePOJO.class);
            if (responsePOJOEntity.getBody() == null) {
                LOGGER.error("Sunrise/Sunset API returned with no body");
                throw new InternalServerError();
            }
            if (!"OK".equalsIgnoreCase(responsePOJOEntity.getBody().getStatus())) {
                LOGGER.error(String.format("Sunrise/Sunset API returned non OK status: %s", responsePOJOEntity.getBody().getStatus()));
                throw new InternalServerError();
            }

            APIResultPOJO apiResultPOJO = responsePOJOEntity.getBody().getResult();
            return SunriseSunsetDTO.builder()
                    .sunriseTime(apiResultPOJO.getSunrise())
                    .sunsetTime(apiResultPOJO.getSunset())
                    .civilTwilightBeginTime(apiResultPOJO.getCivilTwilightBeginTime())
                    .civilTwilightEndTime(apiResultPOJO.getCivilTwilightEndTime())
                    .nauticalTwilightBeginTime(apiResultPOJO.getNauticalTwilightBeginTime())
                    .nauticalTwilightEndTime(apiResultPOJO.getNauticalTwilightEndTime())
                    .astronomicalTwilightBeginTime(apiResultPOJO.getAstronomicalTwilightBeginTime())
                    .astronomicalTwilightEndTime(apiResultPOJO.getAstronomicalTwilightEndTime())
                    .build();
        } catch (DateTimeParseException e) {
            LOGGER.error("Sunrise/Sunset API changed date format in their response", e);
            throw new InternalServerError();
        }
    }
}
