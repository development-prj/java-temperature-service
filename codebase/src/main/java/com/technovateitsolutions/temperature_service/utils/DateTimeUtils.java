package com.technovateitsolutions.temperature_service.utils;

import java.time.LocalDateTime;
import java.util.Optional;

public class DateTimeUtils {
    public static Optional<Double> datetimeToElapsedSecondsOfDay(LocalDateTime input) {
        if (input == null) {
            return Optional.empty();
        }
        return Optional.of(input.getHour() * 3600d + input.getMinute() * 60 + input.getSecond());
    }
}
