package com.technovateitsolutions.temperature_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR,
        reason = "Something went wrong. Our trained monkeys are working to fix it asap.")
public class InternalServerError extends RuntimeException { }
