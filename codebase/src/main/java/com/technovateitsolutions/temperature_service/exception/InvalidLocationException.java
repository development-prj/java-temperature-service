package com.technovateitsolutions.temperature_service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidLocationException extends ResponseStatusException {
    public InvalidLocationException(String reason) {
        super(HttpStatus.BAD_REQUEST, reason);
    }
}
