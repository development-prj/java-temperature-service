package com.technovateitsolutions.temperature_service.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class SunriseSunsetDTO {
    private float latitude;
    private float longitude;

    private LocalDateTime currentTime;
    private TwilightType twilightPreference = TwilightType.CIVIL;

    private LocalDateTime sunriseTime;
    private LocalDateTime sunsetTime;

    private LocalDateTime civilTwilightBeginTime;
    private LocalDateTime civilTwilightEndTime;
    private LocalDateTime nauticalTwilightBeginTime;
    private LocalDateTime nauticalTwilightEndTime;
    private LocalDateTime astronomicalTwilightBeginTime;
    private LocalDateTime astronomicalTwilightEndTime;
}