package com.technovateitsolutions.temperature_service.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ScreenColorDTO {
    private double temperature;
}
