package com.technovateitsolutions.temperature_service.dto;

public enum TwilightType {
    CIVIL,
    NAUTICAL,
    ASTRONOMICAL
}
