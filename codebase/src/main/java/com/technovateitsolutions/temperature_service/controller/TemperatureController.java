package com.technovateitsolutions.temperature_service.controller;

import com.technovateitsolutions.temperature_service.dto.ScreenColorDTO;
import com.technovateitsolutions.temperature_service.dto.TwilightType;
import com.technovateitsolutions.temperature_service.exception.InternalServerError;
import com.technovateitsolutions.temperature_service.service.ITemperatureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class TemperatureController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TemperatureController.class);

    @Autowired
    private ITemperatureService temperatureService;

    @GetMapping("/night-time-temperature")
    public ScreenColorDTO getScreenColorByLocation(@RequestParam("lat") float latitude,
                                                   @RequestParam("lng") float longitude,
                                                   @RequestParam(value = "twilight_type",required = false, defaultValue = "CIVIL") TwilightType twilightType) {
        Optional<Double> screenColorOpt = temperatureService.getScreenColorByLocation(latitude, longitude, twilightType);

        return ScreenColorDTO.builder()
                .temperature(screenColorOpt.get())
                .build();
    }

    @ExceptionHandler({Throwable.class})
    public void handleException(Throwable t) {
        LOGGER.error("A generic error has occurred", t);
        throw new InternalServerError();
    }
}
