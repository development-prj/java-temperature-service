package com.technovateitsolutions.temperature_service.cucumber.utils;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;


public class RestUtil {
    public static Response getResponse(String apiUrl) {
        return RestAssured.get(apiUrl);
    }

    public static JsonPath getJsonPath(Response response) {
        String json = response.asString();
        return new JsonPath(json);
    }

    private static int identifyStatusCode(String responseMessage) {
        int status;

        switch (responseMessage.toLowerCase()) {
            case "ok":
            case "up":
                status = 200;
                break;
            case "bad request":
                status = 400;
                break;
            case "not found":
                status = 404;
                break;
            default:
                status = 0;
        }
        return status;
    }

    public static void checkStatus(Response response, String statusMessage) {
        Assert.assertEquals("The actual status code is: " + response.getStatusCode()
                + " but the expected status code is: " + identifyStatusCode(statusMessage), response.getStatusCode(), identifyStatusCode(statusMessage));
    }

    public static void checkHealthStatus(Response response, String expectedStatus) {
        Assert.assertEquals("The health request failed!", 200, response.getStatusCode());
        JsonPath healthStatusJson = RestUtil.getJsonPath(response);
        String actualStatus = healthStatusJson.get("status");

        Assert.assertEquals("The health status is: " + actualStatus + " but the expected one is: " + expectedStatus, actualStatus, expectedStatus);
    }
}