package com.technovateitsolutions.temperature_service.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
        features = {"src/test/resources/features/temperature.feature"},
        glue = {"com.technovateitsolutions.temperature_service.cucumber"}
)
public class CucumberRunnerTest {

}