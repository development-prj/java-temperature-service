package com.technovateitsolutions.temperature_service.cucumber;

import java.util.ResourceBundle;

public class TestConfiguration {
    private String host;
    private String hostHealthCheck;
    private ResourceBundle defaultTestConfiguration = ResourceBundle.getBundle("configuration");

    public TestConfiguration() {
        if (System.getProperty("host") != null){
            setHost(System.getProperty("host"));
        }
        else {
            setHost(defaultTestConfiguration.getString("host"));
        }

        if (System.getProperty("hostHealthCheck") != null){
            setHostHealthCheck(System.getProperty("hostHealthCheck"));
        }
        else {
            setHostHealthCheck(defaultTestConfiguration.getString("hostHealthCheck"));
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHostHealthCheck() {
        return hostHealthCheck;
    }

    public void setHostHealthCheck(String hostHealthCheck) {
        this.hostHealthCheck = hostHealthCheck;
    }
}
