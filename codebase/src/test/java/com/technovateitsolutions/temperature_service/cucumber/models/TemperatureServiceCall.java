package com.technovateitsolutions.temperature_service.cucumber.models;

import io.restassured.RestAssured;
import lombok.Data;

@Data
public class TemperatureServiceCall {
    private float lat;
    private float lng;
    private double screenColor;

    public String getScreenColorURL() {
        return String.format("%s?lat=%f&lng=%f", RestAssured.baseURI + RestAssured.basePath, this.lat, this.lng);
    }
}
