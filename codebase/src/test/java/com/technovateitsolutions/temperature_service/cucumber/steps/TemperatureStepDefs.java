package com.technovateitsolutions.temperature_service.cucumber.steps;

import com.technovateitsolutions.temperature_service.cucumber.models.TemperatureServiceCall;
import com.technovateitsolutions.temperature_service.cucumber.utils.RestConfig;
import com.technovateitsolutions.temperature_service.cucumber.utils.RestUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;

public class TemperatureStepDefs extends MainStepDefinition {
    private TemperatureServiceCall temperatureServiceCall;
    private Response response;

    public TemperatureStepDefs() {
        this.temperatureServiceCall = new TemperatureServiceCall();
        RestConfig.setBaseURI(testConfiguration.getHost());
        RestConfig.setBasePath("temperaturePath");
    }

    @Given("I want to receive screen color {double} of location with latitude {float} and longitude {float}")
    public void iWantToReceiveScreenColorScreenColorOfLocationWithLatitudeLatAndLongitudeLng(double screenColor, float lat, float lng) {
        temperatureServiceCall.setScreenColor(screenColor);
        temperatureServiceCall.setLat(lat);
        temperatureServiceCall.setLng(lng);
    }

    @When("I sent the request")
    public void iSendTheRequest() {
        response = RestUtil.getResponse(temperatureServiceCall.getScreenColorURL());
    }

    @Then("I receive http status {string}")
    public void iReceiveHttpStatus(String status) {
        RestUtil.checkStatus(response, status);
    }

    @And("I receive content type {string}")
    public void iReceiveContentType(String contentType) {
        Assert.assertEquals(contentType, response.getContentType());
    }

    @And("I check received screen color against the one i asked for")
    public void iCheckReceivedScreenColorAgainstTheOneIAskedFor() {
        JsonPath jsonPathEvaluator = response.jsonPath();
        float screenColor = jsonPathEvaluator.get("temperature");
        Assert.assertEquals(temperatureServiceCall.getScreenColor(), screenColor, 0.01);
    }
}
