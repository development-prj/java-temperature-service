package com.technovateitsolutions.temperature_service.cucumber.utils;

import io.restassured.RestAssured;

import java.util.ResourceBundle;

public class RestConfig {
    private static ResourceBundle endpoints = ResourceBundle.getBundle("endpoints");

    public static void setBaseURI(String environment) {
        RestAssured.baseURI = environment;
    }

    public static void setBasePath(String endpoint) {
        RestAssured.basePath = endpoints.getString(endpoint);
    }
}
