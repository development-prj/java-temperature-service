Feature: Validation of functionality provided by Temperature API
  Scenario Outline: Get the screen color based on location
    Given I want to receive screen color <screen-color> of location with latitude <lat> and longitude <lng>
    When I sent the request
    Then I receive http status "ok"
    And I receive content type "application/json"
    And I check received screen color against the one i asked for
    Examples:
      | lat  |  lng  | screen-color |
      | -44  |   26  |    6000      |
      |  50  |  130  |    2700      |
      |  30  |   70  |    3383.01   |
      |  50  | -100  |    3862.84   |

  Scenario Outline: Provide latitude/longitude outside valid range
    Given I want to receive screen color <screen-color> of location with latitude <lat> and longitude <lng>
    When I sent the request
    Then I receive http status "bad request"
    And I receive content type "application/json"
    Examples:
      |  lat  |  lng  | screen-color |
      | -310  |   26  |    6000      |
      |  200  |  -40  |    6000      |
      |  -60  | -181  |    6000      |
      |   50  |  181  |    6000      |