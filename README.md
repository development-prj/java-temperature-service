# homework-back-end
Technical interview homework assignment for back-end developers

# NightTimeTemperature Microservice

Implement NightTimeTemperature microservice, which determines colour temperature of the screen according to current position of the sun. Calculate temperature in Kelvins according to Fig. 1

![Alt text](./fig1.svg)


Implement a GET request with the following parameters:
- lat (float): Latitude in decimal degrees. Required.
- lng (float): Longitude in decimal degrees. Required.

## Sample request
localhost:8080/night-time-temperature?lat=43.66258321585993&lng=-79.39152689466948

## Response 
{
	“temperature”:3400
}

## Implementation
- Use https://start.spring.io/ to create Gradle Project for Java or Kotlin;
- Use https://sunrise-sunset.org/api for determining sunrise/sunset/twilight;
- Create Dockerfile to run microservice as a Docker container;
- Add JUnit **integration** tests to verify correctness of service execution (test should be executed using Gradle integrationTest task, see https://docs.gradle.org/current/userguide/java_testing.html#sec:configuring_java_integration_tests);
- Create Service Virtualisation to mock sunrise-sunset.org API using [WireMock](http://wiremock.org/);
- WireMock should run as a Docker container as well - use https://hub.docker.com/r/rodolpheche/wiremock/;
- Provide two Spring Boot profiles - dev and qa. Dev profile should use WireMock and qa profile should use real sunrise-sunset.org service;
- Use docker-compose to define and run NightTimeTemperature service and WireMock containers together;

Please provide instructions on how to build, run, and execute integration tests in the following section.

## Execution Instructions

> **Note:** Please provide instructions here

## Locally

### Build
Prerequisites:
- Make sure to install `Gradle 7.x+`, `Java 11`

Steps:
- move to `codebase` directory
- run `gragle build` command

### Run
- locate and move to `build/lib` directory
- export env variable called `SPRING_PROFILES` with value `dev` / `qa` etc.
- replace jar name in following command and execute it <br> `java -Dspring.profiles.active=${SPRING_PROFILES} -jar temperature-service-X.Y.Z[-SNAPSHOT].jar`

## Using Docker

- Make sure to install Docker Server and have the following cli in your path: `docker` and `docker-compose`
- move to `devops/docker` directory <br>
- If you work on a windows machine, you should go over `docker-compose.yml` file and check file mounting paths


### Build
- run `docker-compose build` command

### Run
- run `docker-compose up -d` command. <br><br> This will run three containers: <br> 
	- app with `qa` profile on port `8080`
	- app with `dev` profile on port `8081`
	- wiremock container on port `9000`
	
## Run integration tests
Prerequisites
- Make sure to install `Gradle 7.x+`, `Java 11`, `Docker`
- make sure you started the two docker compose services:
	- `sunrise-sunset-wiremock-api`
	- `temperature_service-dev`

Steps
- move to `codebase` directory
- run `gradle integrationTest` to run integration tests. <br><br>They will NOT automatically run on build time, this is the way to run them.